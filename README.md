![Build Status](https://gitlab.com/inkscape/inkscape-docs/awesome-web-pages/badges/master/pipeline.svg?ignore_skipped=true
)

# Inkscape Documentation's Awesome Web Pages

This repository is a home for all our one-time static pages needs. It can be used for things that would be difficult to add to the inkscape.org website, due to JS/CSS, like slideshows.

### View the website at https://inkscape.gitlab.io/inkscape-docs/awesome-web-pages
